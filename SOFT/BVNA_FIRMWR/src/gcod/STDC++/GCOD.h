//GCODE LIB
#ifndef GCOD_H_INCLUDED
#define GCOD_H_INCLUDED

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>
#include <sstream>
#include <stdlib.h>
#include <vector>

//REV 5
//Jose Luis Marqués Fernández 2020

class GCODE{//GCODE SPEC
private:
int FIND_IDENT(std::string IDEN, std::string STRING);//Internal function for find identificator positions inside strings.
    
public:

    //Variable declaration
    
    //Strings declaration. Messages to decode and messages coded
std::string dec="";//Last message to decode.
std::string cod="";//Last message coded.

//State variables
std::vector<bool> G(200,0);//Sets of G codes states. All to 0.
std::vector<bool> M(200,0);//Sets of M codes states. All to 0.

/*bool G_90=true;//Modo G90 activo
bool G_91=false;//Modo G91 desactivado
bool G_92=false;//Instrucci�n de poner a 0 no activada
bool G_28=false;//Instrucci�n G28 no activa

bool M100=false;//Instrucción de medición de muestra, mide la muestra y se envía por USB el dato leído*/


//Value variables double/float/long/int
std::vector<double> X={0};//X axis position. Multiple X axis avaliable for robotic platforms
std::vector<double> Y={0};//Y axis position. Multiple Y axis avaliable for robotic platforms
std::vector<double> Z={0};//Z axis position. Multiple Z axis avaliable for robotic platforms

std::vector<float> pX={0};//X axis position precision. Multiple X axis avaliable for robotic platforms
std::vector<float> pY={0};//Y axis position precision. Multiple Y axis avaliable for robotic platforms
std::vector<float> pZ={0};//Z axis position precision. Multiple Z axis avaliable for robotic platforms


std::vector<double> U={0};//U axis rotation. Multiple U axis avaliable for robotic platforms
std::vector<double> V={0};//V axis rotation. Multiple V axis avaliable for robotic platforms
std::vector<double> W={0};//W axis rotation. Multiple W axis avaliable for robotic platforms

std::vector<float> pU={0};//U axis rotation precision. Multiple U axis avaliable for robotic platforms
std::vector<float> pV={0};//V axis rotation precision. Multiple V axis avaliable for robotic platforms
std::vector<float> pW={0};//W axis rotation precision. Multiple W axis avaliable for robotic platforms

std::vecto<std::vector<double>> SNS={{0}};//Sensor vector. Recomended [0,n] machine comMon sensors, and [a,n] for axis especific sensor, [s*a,n] for axis set.
std::vecto<std::vector<<float>> pSNS={{0}};//Sensor precision vector.
 
 std::vecto<std::vector<double>> GPA={{0}};//General porpouse actuator.
std::vecto<std::vector<float>> pGPA={{0}};//General porpouse actuator precision.


int INST=0; //Estado de instrucci�n, 0 completado, 1 ejecutandose.

    GCODE();//Constructor sin estado
    GCODE(bool g90,bool g91, bool g92, bool g28);//Constructor con estado
    GCODE(const GCODE& gc);//Duplicador de clase
    GCODE& operator=(const GCODE& gc); //Igualador de clase
	~GCODE();//Destructor
    
    void SETP();
    
	void DECODE(String &msg,double &xd, double &spd, int &tmm);//Decodificador de instrucciones, mensaje recibido, posición[mm], velocidad[mm/s], tiempo de medida del sensor[s*10^-3]
    
    void CODE(String denm, double& x);
    
    void CODE(String denm, double& x);

    //BASE GCOD CODEC_DECODEC
    double DEc(std::string ID, std::string MESS);//Returns the value on the string with the identificaror defined
    
    std::string COd(std::string ID, double VALUE);//Generates a string whith the data codificated
    
    //BASE GCOD CODEC_DECODEC
    
};
#endif

/*HASS MILL/LATHE IMPLEMENTATION 
 * 
 //G CODES
 //https://www.hdknowledge.com/2020/05/haas-cnc-g-code-list-for-lathe-milling.html
 
G00=G[0]    Rapid traverse  Lathe & Milling
G01=G[1]    Linear Interpolation (machining in straight line)	Lathe & Milling
G01=G[1]	Chamfer & corner rounding	Lathe
G02	Circular interpolation clockwise . ( Arc machining )	Lathe & Milling
G03	Circular interpolation counter-clockwise (Arc machining)	Lathe & Milling
G04	Dwell time	Lathe & Milling
G05	Fine spindle control	Lathe
G09	Exact stop	Lathe & Milling
G10	Programmable offset  setting	Lathe & Milling
G12	CW Circular Pocket (Yasnac) [Clockwise]	Milling
G13	CCW Circular Pocket (Yasnac) [counter clockwise]	Milling
G14	Main spindle shift  to sub spindle	Lathe
G15	 Cancel G14 Code	Lathe
G17	Selection of XY plane	Milling
G18	Selection of XZ plane	Lathe & Milling
G19	Selection of ZY plane	Milling
G20	All dimension of program in “Inches”.	Lathe & Milling
G21	All dimension of program in “mm”.	Lathe & Milling
G28	Return to home position (Reference point )	Lathe & Milling
G29	Move to Location Through G29 Reference	Lathe & Milling
G31	Feed skip function	Lathe & Milling
G32	
Thread cutting	Lathe
G35	Automatic Tool Diameter Measurement	Milling
G36	Automatic Work Offset Measurement	Milling
G37	Automatic Tool Length Measurement	Milling
G40	Tool nose compensation cancel (Use to cancel code G41 & G42)	Lathe & Milling
G41	Tool nose compensation left	Lathe & Milling
G42	Tool nose compensation right	Lathe & Milling
G43	Tool length compensation +	Milling
G44	Tool length compensation -	Milling
G47	Engrave line of text	Milling
G49	Cancel G43 & G44 code in milling	Milling
G50	Spindle speed maximum rpm limit	Lathe
G50	Cancel G51 code in milling	Milling
G51 Return to machine zero , cancel offset	Lathe
G51	Scaling	Milling
G52	Global Work Offset Coordinate System Shift	Lathe
G52	Work offset Positioning Coordinate	Lathe
G52	Set local coordinate system	Milling
G53	Machine zero position coordinate	Lathe
G53	Machine coordinate selection non modal	Milling
G54	Work offset coordinate position 1	Lathe & Milling
G55	Work offset coordinate position 2	Lathe & Milling
G56	Work offset coordinate position 3	Lathe & Milling
G57	Work offset coordinate position 4	Lathe & Milling
G58	Work offset coordinate position 5	Lathe & Milling
G59	Work offset coordinate position 6	Lathe & Milling
G60	Unidirectional  positioning	Milling
G61	Exact stop modal	Lathe & Milling
G64	Cancel G61 code	Lathe & Milling
G65	Micro sub-routine call	Lathe & Milling
G68	Rotation	Milling
G69	Cancel G68 code	Milling
G70	Finishing cycle	Lathe
G70	Bolt Hole Circle (Yasnac)	Milling
G71	Bolt Hole Arc (Yasnac)	Milling
G71	O.D. / I.D. stock removal cycle	Lathe
G72	Cancel G71 code Lathe
G72	Bolt holes along an angle (Yasnac)	Milling
G73	Irregular path stock removal cycle	Lathe
G73	High speed peck drill canned cycle	Milling
G74	High speed peck drill cycle OR Face grooving	Lathe
G74	Reverse tap canned cycle	Milling
G75	Peck grooving cycle I.D or O.D.	Lathe
G76	Threading cycle , multiple pass I.D. or O.D.	Lathe
G76	Fine boring canned cycle	Milling
G77	Flatting cycle (live tool)	Lathe
G77	Back bore canned cycle	Milling
G80	 Cancel canned cycle	Lathe & Milling
G81	Drill canned cycle	Lathe & Milling
G82	Spot  drill canned cycle	Lathe & Milling
G83	Peck drill canned cycle modal	Lathe & Milling
G84	Tapping canned cycle	Lathe & Milling
G85	Boring canned cycle (IN or OUT)	Lathe & Milling
G86	Bore in , stop , rapid out  canned cycle	Lathe & Milling
G87	Bore in , stop , manual retract  canned cycle	Lathe & Milling
G88	Bore in , dwell , manual retract  canned cycle	Lathe & Milling
G89	Bore In ,Dwell, Bore out Canned Cycle modal	Lathe
G89	Bore canned cycle	Milling
G90	I.D./O.D. turning cycle	Lathe
G90	Absolute	Milling
G91	Incremental	Milling
G92	Threading cycle modal	Lathe
G92	Set work coordinate	Milling
G93	Inverse time feed mode ON	Milling
G94	Inverse time feed mode OFF	Milling
G94	End facing cycle	Lathe
G95	Live tooling end face rigid tap modal	Lathe
G96	Constant surface speed	Lathe
G97	Constant non varying spindle speed	Lathe
G98	Feed per minute	Lathe
G98	Initial point return	Milling
G99	Feed per revolution	Lathe
G99	R (retraction) plane return	Milling
G100	Disable mirror image (cancel g101)	Lathe & Milling
G101	Enable mirror image	Lathe & Milling
G102	Programmable output to RS-232	Lathe & Milling
G103	Block look ahead limit	Lathe & Milling
G105	Servo bar command	Lathe
G107	Cylindrical mapping	Milling
G110	Selection coordinate system 7	Lathe & Milling
G111	Selection coordinate system8	Lathe & Milling
G112	Selection coordinate system9	Milling
G112	Cartesian to polar transformation	Lathe
G113	Cartesian to polar transformation cancel (cancel G112)	Lathe
G113	Selection coordinate system 10	Milling
G114	Selection coordinate system 9	Lathe
G114	Selection coordinate system 11	Milling
G115	Selection coordinate system 12	Milling
G116	Selection coordinate system 13	Milling
G117	Selection coordinate system 14	Milling
G118	Selection coordinate system 15	Milling
G119	Selection coordinate system 16	Milling
G120	Selection coordinate system 17	Milling
G121	Selection coordinate system 18	Milling
G122	Selection coordinate system 19	Milling
G123	Selection coordinate system 20	Milling
G124	Selection coordinate system 21	Milling
G125	Selection coordinate system 22	Milling
G126	Selection coordinate system 23	Milling
G127	Selection coordinate system 24	Milling
G128	Selection coordinate system 25	Milling
G129	Selection coordinate system 26	Milling
G129	Selection coordinate system 24	Lathe
G136	Automatic  work offset center measurement	Milling
G141	3D + cutter compensation	Milling
G143	Tool length compensation for five axis	Milling
G150	General purpose pocket milling	Milling
G153	5 axis high speed peck drilling canned cycle	Milling
G154	Select Work Offset Positioning Coordinate P1-99	Lathe
G154	P1-P99  Replaces G110-G129 on newer machines	Milling
G155	5 Axis Reverse Tapping Canned Cycle	Milling
G159	Background Pickup / Part Return	Lathe
G160	APL Axis Command On	Lathe
G161	APL Axis Command OFF	Lathe
G161	Drill canned cycle for 5 axis	Milling
G162	5 Axis Spot Drill / Counter bore Canned Cycle	Milling
G163	Peck drill canned cycle for 5 Axis (setting 22)	Milling
G164	Tapping canned cycle for 5 axis	Milling
G165	Bore in , bore out canned cycle for 5 axis	Milling
G166	Bore in , stop , rapid out canned cycle  for 5 axis	Milling
G169    Bore , dwell , bore out canned cycle for 5 axis	Milling
G174	Special Purpose Non-Vertical Rigid Tapping CCW	Milling
G184	Special Purpose Non-Vertical Rigid Tapping CW	Milling
G184	Reverse tapping canned cycle	Lathe
G186	Live tooling reverse rigid tap	Lathe
G187	Accuracy control for high speed machining	lathe &Milling
G188	Get program from program schedule table	Milling
G194	Sub spindle , tapping canned cycle	Lathe
G195	Live tooling redial tapping	Lathe
G196	Live tooling redial tapping reverse	Lathe
G200	Index on the fly	Lathe 
 
 //M CODES
 //https://www.hdknowledge.com/2020/05/haas-cnc-m-code-list-for-lathe-milling.html
 
M00 Program stop    Lathe& Milling
M01 Optional code for program stop	Lathe& Milling
M02	End of program	Lathe& Milling
M03	Spindle start clockwise	Lathe& Milling
M04	Spindle start counter clockwise	Lathe& Milling
M05	Spindle stop (cancel M03 & M04)	Lathe& Milling
M06	Tool change command	Milling
M08	Coolant ON	Lathe& Milling
M09	Coolant OFF	Lathe& Milling
M10	Chuck clamp	Lathe
M10	4th axis break ON	Milling
M11	Chuck unclamp	Lathe
M11	4th axis break release	Milling
M12	Auto air jet ON	Lathe
M12	5th axis break ON	Milling
M13	Auto air jet OFF	Lathe
M13	5th axis break release	Milling
M14	Main spindle clamp	Lathe
M15	Main spindle unclamp	Lathe
M16	Tool change command	Milling
M17	Rotate turret forward	Lathe
M18	Rotate turret reverse	Lathe
M17	APC Pallet Unclamp and Open APC Door	Milling
M18	APC Pallet clamp and close APC Door	Milling
M19	Orient spindle (P,R)	Lathe& Milling
M21	Tailstock advance	Lathe
M22	Tailstock retract	Lathe
M21	Interface with - fin signals (optional user Mcode)	Milling
M23	Angle out of thread ON	Lathe
M24	Angle of thread OFF	Lathe
M25	Interface with - fin signals optional user M code	Lathe
M28	Interface with - fin signals optional user M code	Lathe& Milling
M30	Main program end & reset	Lathe& Milling
M31	Chip auger forward	Lathe& Milling
M32	Chip auger reverse	Lathe& Milling
M33	Chip auger stop	Lathe& Milling
M34	Coolant spigot position down , increament (+1)	Milling
M35	Coolant spigot position up , increament (-1)	Milling
M36	Part  catcher ON	Lathe
M37	Part catcher OFF	Lathe
M36	Pallet part ready	Milling
M38	Specify spindle variation ON	Lathe
M39	Specify spindle variation OFF	Lathe
M39	Rotate tool turret	Milling
M41	Spindle low gear override	Lathe& Milling
M42	Spindle high gear override	Lathe& Milling
M43	Turret unlock	Lathe
M44	Turret lock	Lathe
M50	Execute pallet change	Milling
M51-M58	Optional user M code set	Lathe& Milling
M59	Output relay set	Lathe& Milling
M61-M68	Optional User M Code Clear	Lathe& Milling
M69	Output relay clear	Lathe& Milling
M75	Set G35 or G136 Reference Point	Milling
M76	Control display inactive	Lathe& Milling
M77	Control display active	Lathe& Milling
M78	Alaram if skip signal found	Lathe& Milling
M79	Alaram if skip signal not found	Lathe& Milling
M80	Automatic door open	Milling
M81	Automatic door close	Milling
M82	Tool unclamp	Milling
M83	Auto air jet ON	Milling
M84	Auto air jet OFF	Milling
M85	Automatic door open	Lathe
M86	Automatic door close	Lathe
M86	Tool clamp	Milling
M88	High pressure coolant ON	Lathe& Milling
M89	High pressure coolant OFF	Lathe& Milling
M93	Axis position capture start	Lathe& Milling
M94	Axis position capture stop	Lathe& Milling
M95	Sleep mode	Lathe& Milling
M96	Jump if no input	Lathe& Milling
M97	Local sub program call	Lathe& Milling
M98	Sub program call	Lathe& Milling
M99	Sub program /Routine return/ Loop program	Lathe& Milling
M101	MOM (minimum oil machining ) canned cycle mode (I)	Milling
M102	MOM (minimum oil machining) mode (I , J)	Milling
M103	MOM (minimum oil machining) mode CANCEL	Milling
M109	Interactive user input	Lathe& Milling
M110	Tailstock chuck clamp	Lathe
M111	Tailstock chuck unclamp	Lathe
M119	Sub spindle orient	Lathe
M21-M28	Optional user M code interface with M-Fin Signal 	Lathe
M133	Live tool drive forward	Lathe
M134	Live tool drive reverse	Lathe
M135	Live tool drive stop	Lathe
M143	Sub spindle forward	Lathe
M144	Sub spindle reverse	Lathe
M145	Sub spindle stop	Lathe
M154	C axis engage	Lathe
M155	C axis disengage	Lathe
M164	Rotate APL grippers to “n” position 	Lathe
M165	Open APL gripper 1 (Raw Material)	Lathe
M166	Close APL gripper 1 (Raw Material)	Lathe
M167	Open APL gripper 2 (finish material)	Lathe
M168	Close  APL gripper 2 (finish material)	Lathe 
 */

/*RepRap IMPLEMENTATION
  //G CODES
 
 //M CODES
 
 */
